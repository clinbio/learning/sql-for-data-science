# 17. Set operations with UNION, INTERSECT and EXCEPT

## Set operations

In SQL, set operations allow you to combine the results of two or more SELECT queries into a single result. They are
very powerful tools that can be used to solve complex problems with ease.

## `UNION` operator

The UNION operator is used to combine the results of two or more SELECT statements into a single result set. It removes
duplicate rows between the various SELECT statements. The syntax for using the UNION operator is:

```sql
SELECT
  column_name(s)
FROM
    table1
UNION
SELECT
  column_name(s)
FROM
    table2;
```

Example:

```sql
SELECT
  first_name,
  last_name
FROM
    customers
UNION
SELECT
  first_name,
  last_name
FROM
    staffs;
```

This query retrieves all first_name and last_name values from the customers and employees tables and combines them into a single result set.

## `INTERSECT` operator

The INTERSECT operator is used to combine the results of two or more SELECT statements into a single result set. It returns only the rows that appear in all the result sets. The syntax for using the INTERSECT operator is:

```sql
SELECT
  column_name(s)
FROM
    table1
INTERSECT
SELECT
  column_name(s)
FROM
    table2;
```

Example:

```sql
SELECT
  last_name
FROM
  customers
INTERSECT
SELECT
  UPPER(last_name)
FROM
  staffs;
```

This query retrieves all last_name values from the customers and staffs tables and returns only the rows that appear in both tables.

## `EXCEPT` operator

The EXCEPT operator is used to combine the results of two SELECT statements into a single result set. It returns only the rows in the first SELECT statement that are not in the second SELECT statement. The syntax for using the EXCEPT operator is:

```sql
SELECT
  column_name(s)
FROM
    table1
EXCEPT
SELECT
  column_name(s)
FROM
    table2;
```

Example:

```sql
SELECT
  UPPER(last_name)
FROM
    customers
EXCEPT
SELECT
    last_name
FROM
    staffs;
```

This query retrieves all last_name values from the staffs table that are not in the customers table.