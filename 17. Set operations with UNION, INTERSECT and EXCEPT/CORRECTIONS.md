# Set operations with UNION, INTERSECT and EXCEPT

## Exercises (~5 min)

**Which of these first names are found among the actors and the customers? LAURY, LAURA, LAUREN and LAURYN**

Answer:

| first_name |
| ---------- |
| LAURA      |
| LAUREN     |

```sql
SELECT
  first_name
FROM
  actors
WHERE
  first_name IN ('LAURY', 'LAURA', 'LAUREN', 'LAURYN')
UNION
SELECT
  first_name
FROM
  customers
WHERE
  first_name IN ('LAURY', 'LAURA', 'LAUREN', 'LAURYN');
```

**Is there an actor who is also a customer?**

Answer: Yes

| first_name | last_name |
| ---------- | --------- |
| JENNIFER   | DAVIS     |

```sql
SELECT
  first_name,
  last_name
FROM
  actors 
INTERSECT
SELECT
  first_name,
  last_name
FROM
  customers;
```

**How many actors first names are never found among customers?**

Answer: 56

```sql
SELECT
  first_name
FROM
    actors
EXCEPT
SELECT
  first_name
FROM
    customers;
```