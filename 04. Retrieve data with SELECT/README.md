# Retrieve data with SELECT

## The SQL SELECT Statement

A `SELECT` statement in SQL is used to query or retrieve data from one or more tables in a relational database.
The `SELECT`
statement allows you to specify the columns you want to retrieve, the tables from which to retrieve the data, and
conditions that the data must meet in order to be included in the result set.

The basic structure of a `SELECT` statement is as follows:

```sql
SELECT 
    column1, column2, ... 
FROM 
    table_name;
```

Here's a breakdown of the components:

- `SELECT`: The keyword used to indicate that you want to retrieve data.
- `column1, column2, ...`: A comma-separated list of columns to retrieve from the table.
- `FROM`: The keyword used to specify the table from which to retrieve the data.
- `table_name`: The name of the table containing the data you want to query.

For example, consider the following `SELECT` statement:

```sql
SELECT
    first_name, last_name
FROM
    staffs;
```

Results:

| first_name | last_name |
|------------|-----------|
| Mike       | Hillyer   |
| Jon        | Stephens  |

This query retrieves the `first_name` and `last_name` columns from the `staff` table for all staff members.

When retrieving multiple columns from a table, it is possible to use `*` which will automatically select all fields
available in the table.

For example:

```sql
SELECT
    *
FROM
    staffs;
```

Results:

| staff_id | first_name | last_name | address_id | picture | email                        | store_id | active | username | password                                 | last_update         |
|----------|------------|-----------|------------|---------|------------------------------|----------|--------|----------|------------------------------------------|---------------------|
| 1        | Mike       | Hillyer   | 3          |         | Mike.Hillyer@sakilastaff.com | 1        | 1      | Mike     | 8cb2237d0679ca88db6464eac60da96345513964 | 2020-12-23 07:12:31 |
| 2        | Jon        | Stephens  | 4          |         | Jon.Stephens@sakilastaff.com | 2        | 1      | Jon      | 8cb2237d0679ca88db6464eac60da96345513964 | 2020-12-23 07:12:31 |