# Retrieve data with SELECT

## Open sakila.db

Make sure that you downloaded locally the
database `sakila`: https://gitlab.sib.swiss/clinbio/learning/sql-for-data-science/-/raw/master/sakila.db

Open Beekeeper Studio and you should be welcomed with a screen asking you to configure a **New Connection**.

In `Connection Type`, select `SQLite`. Then click on **Choose File** and select the freshly downloaded `sakila.db`.

If you would like to save the connection, fill the `Connection Name` field and click on **Save**.

Finally, connect to `sakila.db` by clicking on **Connect**.

## Quick introduction

Beekeeper Studio is divided into three sections. On the left, you have your open database with its tables. On the right
top, you have your query editor and below, you have the results.

If you want to inspect a specific table, simply double-click on one of the tables on the left and it should open a new tab with the content of the table.

## Exercises (~5 min)

**Is there a staff member called "Mike Hillyer"?**

Answer:

**Is `Music` a valid category?**

Answer:

**How many languages exist?**

Answer:
