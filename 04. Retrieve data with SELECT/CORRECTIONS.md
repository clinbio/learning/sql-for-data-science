# Retrieve data with SELECT

## Corrections

**Is there a staff member called "Mike Hillyer"?**

Answer: Yes

```sql
SELECT
    *
FROM
    staffs;
```

**Is `Music` a valid category?**

Answer: Yes

```sql
SELECT
    name
FROM
    categories;
```

**How many languages exist?**

Answer: 6

```sql
SELECT
    *
FROM
    languages;
```

Interestingly, we can also use `COUNT(*)` to count the number of rows in a table.

```sql
SELECT
    COUNT(*)
FROM
    languages;
```