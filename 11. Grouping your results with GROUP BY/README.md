# 11. Grouping your results with GROUP BY

The GROUP BY clause allows using aggregate functions at a finer level than just the whole table. We can then obtain aggretations as per a grouping column, or a set of grouping columns.  

## Simple example

For example, when we want to know how many payments we have got, we would use :  

```sql
SELECT
    COUNT(*)
FROM
    payments;
```

|COUNT(*)|
|--------|
|16049|

The COUNT function go through the selected rows, computes an aggregated value from these rows (here it counts the rows), and returns the result on one single row.  

What if I want to know the number of payments that each of my staff has managed ? Here comes the GROUP BY function :  

```sql
SELECT
    staff_id, COUNT(*)
FROM
    payments
GROUP BY 
    staff_id;
```

|staff_id|COUNT(*)|
|--------|--------|
|1|8057|
|2|7992|

So here, I can see how many payments have been managed by each of our staff_if.  

## Combined example

The GROUP BY clause goes between the WHERE and ORDER BY clauses.  

Let's see now an example using other clauses.  

I want to know how many films of "Action" (id 1), "Horror" (id 11), "Drama" (id 7) and Sci-Fi (id 14) I have. I would like to sort my results from the most common category to the less common.  

It is typically a case where I will have to GROUP BY:  

```sql
SELECT
    category_id,
    COUNT(*)
FROM
    category_film
WHERE
    category_id IN (1, 11, 7, 14)
GROUP BY
    category_id
ORDER BY
    COUNT(*) DESC;
```

|category_id|COUNT(*)|
|-----------|--------|
|1|64|
|7|62|
|14|61|
|11|56|

So we see that the category 1 (Action) is the most represented in our film catalog.  

## GOUP BY several columns and create sub groups

Now we are going to display the number of payments by amount and staff_id. This will required to use GROUP BY on two columns :

```sql
SELECT
    staff_id,
    amount,
    COUNT(*)
FROM
    payments
GROUP BY
    staff_id,
    amount
ORDER BY
    staff_id ASC;
```

## Filter on aggregated values with HAVING

When we want to filter query results, we use the `WHERE` keyword and specify our filter. This does not work with aggregated functions. Instead, filtering on aggregated function requires a dedicated keyword: `HAVING`.  

For example, I want to know all films by store for which I have 2 examplaries in my inventory. I acan achieve this as follows:

```sql
SELECT
    film_id,
    store_id,
    COUNT(film_id) AS "Number of films"
FROM
    inventories
GROUP BY
    store_id,
    film_id
HAVING
    COUNT(film_id) < 3
ORDER BY
    film_id;
```

| film_id | store_id | Number of films |
| ------- | -------- | --------------- |
| 7       | 1        | 2               |
| 9       | 2        | 2               |
| 15      | 1        | 2               |
| 16      | 1        | 2               |
| 16      | 2        | 2               |
| 19      | 2        | 2               |
| 21      | 1        | 2               |
...

Notice that the `HAVING` goes after the `GROUP BY`.
