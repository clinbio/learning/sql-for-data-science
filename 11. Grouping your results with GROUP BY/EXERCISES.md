# 11. Grouping your results with GROUP BY

## Exercices (~5 min)

### Looking at the inventory table, how many films do you have in each store ?  

Answer:

### Looking at the inventory table, each store can hold several exemplars of a single film. How many single films do we have in each store ?

Answer:

### I want to know the five best customers i.e. those who paid most. I want to see how many payments this person did, its average payment amount, and how much this person did pay in total. Write the query, add nice column labels, and limit the displayed values to two decimals.

Answer: 