# 11. Grouping your results with GROUP BY

## Corrections

### Looking at the inventory table, how many films do you have in each store ?  

```sql
SELECT
    store_id,
    COUNT(*)
FROM
    inventories
GROUP BY
    store_id;
```

### Looking at the inventory table, each store can hold several exemplars of a single film. How many single films do we have in each store ?

```sql
SELECT
    store_id,
    COUNT(DISTINCT film_id) AS "Single films"
FROM
    inventories
GROUP BY
    store_id;
```

### I want to know the five best customers i.e. those who paid most. I want to see how many payments this person did, its average payment amount, and how much this person did pay in total. Write the query, add nice column labels, and limit the displayed values to two decimals.

```sql
SELECT
    customer_id,
    COUNT(*) AS "Number of payments",
    ROUND(AVG(amount), 2) AS "Average payment",
    SUM(amount) AS "Total paid"
FROM
    payments
GROUP BY
    customer_id
ORDER BY 
    SUM(amount) DESC
LIMIT 5;
```