# Datatypes in SQL

SQL supports a variety of datatypes to store different types of data in a database. The specific datatypes available may
vary slightly between database management systems (DBMS), such as MySQL, SQL Server, Oracle, or PostgreSQL. However,
there are some common datatypes that you'll find across most systems:

## Numbers:

These datatypes are used to store numeric values, including integers and floating-point numbers.

### Integers

- `TINYINT`: Store very small whole numbers with a very small range (0 to 255).
- `SMALLINT`: Stores smaller whole numbers with a smaller range than INTEGER (0 to 65,535).
- `INT` (or `INTEGER`): Stores whole numbers (0 to 4,294,967,295).
- `BIGINT`: Stores larger whole numbers with a larger range than INTEGER (0 to 18,446,744,073,709,551,615).

### Negative numbers

In databases, negative numbers are supported by dedicating one of the bits to the sign (either positive or negative),
with the remaining bits reserved for the value. For example, a `TINYINT` with negative numbers enabled has one bit
dedicated to the sign and seven bits for the value, which means it can store values from -128 to 127.

### Decimals

- `DECIMAL` (or `NUMERIC`): Stores fixed-point data type that stores exact values.
- `FLOAT`: Stores floating-point data type that stores approximate values.
- `DOUBLE`: Stores floating-point data type that stores larger and more precise values than `FLOAT`.

## When to use decimal

If you need to store values that require absolute precision, such as currency or other financial data, you should use
the `DECIMAL` data type. With `DECIMAL`, you can specify the maximum number of digits and how many digits should occur after
the decimal point.

For example, suppose you want to store a maximum of 10 digits, with two digits after the decimal, the syntax would be:

```
DECIMAL(10,2)
```

The first argument specifies the maximum number of digits, while the second argument specifies how many should appear
after the decimal.

## Strings:

These datatypes are used to store character strings.

- `CHAR(n)`: Stores fixed-length strings with a specified length n. The stored string will always be n characters long,
  padded with spaces if necessary.
- `VARCHAR(n)`: Stores variable-length strings with a maximum specified length n. The stored string can be up to n
  characters long, and any unused space is not padded.
- `TEXT`: Stores much longer strings such as long blocks of text.

## Date and Time datatypes

- DATE: Stores date values without a time component (e.g., '2022-12-31').
- TIME: Stores time values without a date component (e.g., '23:59:59').
- TIMESTAMP: Stores both date and time values together (e.g., '2022-12-31 23:59:59').
- DATETIME: Stores both date and time values together, but with a larger range than TIMESTAMP.
- INTERVAL: Stores a duration or period of time, such as the difference between two dates or times.

Note: The main difference between DATETIME and TIMESTAMP is that TIMESTAMP has a more limited range of data, from the year 1970 to 2038-01-19. This limitation is referred to as the "2038 problem".

## Binary datatypes:

These datatypes are used to store binary data, such as images or files.

- BINARY(n): Stores fixed-length binary data with a specified length n.
- VARBINARY(n): Stores variable-length binary data with a maximum specified length n.
- BLOB: Stores large binary data, often used for storing images, audio, or video files.

## Boolean datatypes:

These datatypes are used to store true or false values.

- BOOLEAN (or TINYINT): Stores a single true or false value, represented as 1 or 0, respectively.

## Enumerated and Set datatypes:

These datatypes are used to store a value from a predefined list of values.

- ENUM: Stores one value from a predefined list of values.
- SET: Stores multiple values from a predefined list of values, stored as a comma-separated list.

Please note that the exact names, implementations, and supported features of these datatypes may vary between different
database management systems. It is essential to consult the documentation for the specific DBMS you are using to ensure
that you understand the nuances of its datatype support. For example, SQL Server uses the 'NVARCHAR' datatype for
storing variable-length Unicode character strings, whereas PostgreSQL uses 'TEXT' instead of 'VARCHAR' for storing
variable-length character strings without a specified length limit.

In summary, SQL offers a variety of datatypes to accommodate different types of data, such as numeric, character, date
and time, binary, and boolean values. Understanding the available datatypes in your chosen database management system is
crucial for designing an efficient and effective database schema.

# SQLite

While most DBMS implements most of the datatypes mentioned above, SQLite is a bit different, as it is a lightweight DBMS. It only implements some of the datatypes mentioned above. The following table shows the datatypes supported by SQLite:

| Datatype | Description                                                                                                |
|----------|------------------------------------------------------------------------------------------------------------|
| INTEGER  | The value is a signed integer, stored in 1, 2, 3, 4, 6, or 8 bytes depending on the magnitude of the value |
| TEXT     | The value is a text string, stored using the database encoding (UTF-8, UTF-16BE or UTF-16LE)               |
| BLOB     | The value is a blob of data, stored exactly as it was input                                                |
| REAL     | The value is a floating point value, stored as an 8-byte IEEE floating point number                        |
| NUMERIC  | The value is a numeric value, stored as an 8-byte IEEE floating point number or an integer                 |
