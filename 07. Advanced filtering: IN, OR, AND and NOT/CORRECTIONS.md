# Advanced filtering: IN, OR, AND and NOT

## Corrections

**Which cities are located in the following country_id (34, 38 and 91)?**

Answer:

| city        |
|-------------|
| Brest       |
| Le Mans     |
| Toulon      |
| Toulouse    |
| Duisburg    |
| Erlangen    |
| Halle/Saale |
| Mannheim    |
| Saarbrcken  |
| Siegen      |
| Witten      |
| Basel       |
| Bern        |
| Lausanne    |

```sql
SELECT
  city
FROM
  cities
WHERE
  country_id IN (34, 38, 91);
```

**How many actors are called PENELOPE or CUBA as their first name?**

Answer: 7

```sql
SELECT
  COUNT(*)
FROM
    actors
WHERE
    first_name = 'PENELOPE'
OR
    first_name = 'CUBA';
```

**What is the actor_id of the actor called PENELOPE MONROE?**

Answer: 120

```sql
SELECT
  actor_id
FROM
    actors
WHERE
    first_name = 'PENELOPE'
AND
    last_name = 'MONROE';
```

**How many films were not released in 2006?**

Answer: 0

```sql
SELECT
  COUNT(*)
FROM
    films
WHERE NOT
    release_year = 2006;
```