# Advanced filtering: IN, OR, AND and NOT

## Filtering operators

In SQL, advanced filtering operators are used to filter the results of a query based on specific conditions. These
operators allow you to combine and refine conditions to retrieve the desired data from a database.

## `IN` operator

The IN operator is used to filter data based on whether a specific column's value is part of a given set of values. It
helps you to specify multiple values within a WHERE clause, making it more concise and easier to read. The syntax for
using the IN operator is:

```sql
SELECT
  column_name(s)
FROM
  table_name
WHERE
  column_name IN (value1, value2,...);
```

Example:

```sql
SELECT
  *
FROM
  actors
WHERE
  first_name IN ('PENELOPE', 'NICK', 'ED');
```

This query retrieves all records from the actors table where the first_name is either `PENELOPE`, `NICK` or `ED`.

| actor_id | first_name | last_name | last_update         |
|----------|------------|-----------|---------------------|
| 1        | PENELOPE   | GUINESS   | 2020-12-23 07:12:29 |
| 2        | NICK       | WAHLBERG  | 2020-12-23 07:12:29 |
| 3        | ED         | CHASE     | 2020-12-23 07:12:29 |
| 44       | NICK       | STALLONE  | 2020-12-23 07:12:29 |
| 54       | PENELOPE   | PINKETT   | 2020-12-23 07:12:29 |
| 104      | PENELOPE   | CRONYN    | 2020-12-23 07:12:30 |
| 120      | PENELOPE   | MONROE    | 2020-12-23 07:12:30 |
| 136      | ED         | MANSFIELD | 2020-12-23 07:12:30 |
| 166      | NICK       | DEGENERES | 2020-12-23 07:12:31 |
| 179      | ED         | GUINESS   | 2020-12-23 07:12:31 |

## `OR` operator

The OR operator is used to combine multiple conditions in a WHERE clause. It returns a record if any of the specified
conditions are true. The syntax for using the OR operator is:

```sql
SELECT
  column_name(s)
FROM
  table_name
WHERE
  condition1
  OR condition2...;
```

Example

```sql
SELECT
  *
FROM
  actors
WHERE
  first_name = 'PENELOPE'
  OR first_name = 'ED';
```

This query retrieves all records from the actors table where the first_name is `PENELOPE` or `ED`.

| actor_id | first_name | last_name | last_update         |
|----------|------------|-----------|---------------------|
| 1        | PENELOPE   | GUINESS   | 2020-12-23 07:12:29 |
| 3        | ED         | CHASE     | 2020-12-23 07:12:29 |
| 54       | PENELOPE   | PINKETT   | 2020-12-23 07:12:29 |
| 104      | PENELOPE   | CRONYN    | 2020-12-23 07:12:30 |
| 120      | PENELOPE   | MONROE    | 2020-12-23 07:12:30 |
| 136      | ED         | MANSFIELD | 2020-12-23 07:12:30 |
| 179      | ED         | GUINESS   | 2020-12-23 07:12:31 |

## `AND` operator

The AND operator is used to combine multiple conditions in a WHERE clause. It returns a record only if all the specified
conditions are true. The syntax for using the AND operator is:

```sql
SELECT
  column_name(s)
FROM
  table_name
WHERE
  condition1
  AND condition2...;
```

Example:

```sql
SELECT
  *
FROM
  actors
WHERE
  first_name = 'PENELOPE'
  AND last_name = 'GUINESS';
```

This query retrieves all records from the actors table where the first_name is `PENELOPE` and the last_name
is `GUINESS`.

| actor_id | first_name | last_name | last_update         |
|----------|------------|-----------|---------------------|
| 1        | PENELOPE   | GUINESS   | 2020-12-23 07:12:29 |

## `NOT` operator

The NOT operator is used to negate a condition in a WHERE clause. It returns records where the specified condition is
false. The syntax for using the NOT operator is:

```sql
SELECT
  column_name(s)
FROM
  table_name
WHERE
  NOT condition;
```

Example:

```sql
SELECT
  *
FROM
  actors
WHERE
  NOT first_name = 'PENELOPE';
```

This query retrieves all records from the actors table where the first_name is not `PENELOPE`.

| actor_id | first_name | last_name    | last_update         |
|----------|------------|--------------|---------------------|
| 2        | NICK       | WAHLBERG     | 2020-12-23 07:12:29 |
| 3        | ED         | CHASE        | 2020-12-23 07:12:29 |
| 4        | JENNIFER   | DAVIS        | 2020-12-23 07:12:29 |
| 5        | JOHNNY     | LOLLOBRIGIDA | 2020-12-23 07:12:29 |
| ...      | ...        | ...          | ...                 |
| 200      | THORA      | TEMPLE       | 2020-12-23 07:12:31 |