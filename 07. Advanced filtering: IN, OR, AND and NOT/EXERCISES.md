# Advanced filtering: IN, OR, AND and NOT

## Exercises (~5 min)

**Which cities are located in the following country_id (34, 38 and 91)?**

Answer:

**How many actors are called PENELOPE or CUBA as their first name?**

Answer:

**What is the actor_id of the actor called PENELOPE MONROE?**

Answer:

**How many films were not released in 2006?**

Answer:
