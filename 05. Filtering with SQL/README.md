# Filtering with SQL

## The SQL WHERE clause

The `WHERE` clause in SQL is used to filter the results of a query based on specified conditions. It allows you to
retrieve only the rows from a table that meet the criteria defined by the conditions. The `WHERE` clause is an optional
part of SQL statements like `SELECT`, `UPDATE`, and `DELETE`, and is used to narrow down the data that is affected or
retrieved by the statement.

The `WHERE` clause follows the `FROM` clause in a `SELECT` statement. The conditions in the WHERE clause are created
using comparison operators (such as `=`, `<`, `>`, `<=`, `>=`, `<>`)

Here's an example of a SELECT statement with a WHERE clause:

```sql
SELECT
  *
FROM
  payments
WHERE
  amount > 11;
```

Results:

| payment_id | customer_id | staff_id | rental_id | amount | payment_date            | last_update         |
|------------|-------------|----------|-----------|--------|-------------------------|---------------------|
| 342        | 13          | 2        | 8831      | 11.99  | 2005-07-29 22:37:41.000 | 2020-12-23 07:19:14 |
| 3146       | 116         | 2        | 14763     | 11.99  | 2005-08-21 23:34:00.000 | 2020-12-23 07:19:54 |
| 5280       | 195         | 2        | 16040     | 11.99  | 2005-08-23 22:19:33.000 | 2020-12-23 07:20:25 |
| 5281       | 196         | 2        | 106       | 11.99  | 2005-05-25 18:18:19.000 | 2020-12-23 07:20:25 |
| 5550       | 204         | 2        | 15415     | 11.99  | 2005-08-22 23:48:56.000 | 2020-12-23 07:20:28 |
| 6409       | 237         | 2        | 11479     | 11.99  | 2005-08-02 22:18:13.000 | 2020-12-23 07:20:39 |
| 8272       | 305         | 1        | 2166      | 11.99  | 2005-06-17 23:51:21.000 | 2020-12-23 07:21:07 |
| 9803       | 362         | 1        | 14759     | 11.99  | 2005-08-21 23:28:58.000 | 2020-12-23 07:21:29 |
| 15821      | 591         | 2        | 4383      | 11.99  | 2005-07-07 20:45:51.000 | 2020-12-23 07:22:52 |
| 15850      | 592         | 1        | 3973      | 11.99  | 2005-07-06 22:58:31.000 | 2020-12-23 07:22:53 |

In this example, the `WHERE` clause filters the results to include only rows where the amount column has a value greater
than `11`. The query retrieves all the columns for all payments with an amount greater than `11`.

Here is a list of some primary comparison operators in SQL:

- `=` (Equal): Checks if two values are equal. If they are equal, the condition is true.
*Example: age = 30*
- `<>` or `!=` (Not Equal): Checks if two values are not equal. If they are not equal, the condition is true.
*Example: age <> 30 or age != 30*
- `>` (Greater Than): Checks if the value on the left is greater than the value on the right. If it is, the condition is true.
*Example: age > 30*
- `<` (Less Than): Checks if the value on the left is less than the value on the right. If it is, the condition is true.
*Example: age < 30*
- `>=` (Greater Than or Equal): Checks if the value on the left is greater than or equal to the value on the right. If it is, the condition is true.
*Example: age >= 30*
- `<=` (Less Than or Equal): Checks if the value on the left is less than or equal to the value on the right. If it is, the condition is true.
*Example: age <= 30*