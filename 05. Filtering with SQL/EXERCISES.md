# Filtering with SQL

## Exercises (~5 min)

**How many customers are inactive?**

Answer:

**How many movies did `customer_id` = 318 rent?**

Answer:

**How many payments have an amount `>=` 11.99?**

Answer:

**How many movies do not last more than 46 minutes (included)?**

Answer: