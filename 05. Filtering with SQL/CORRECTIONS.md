# Filtering with SQL

## Corrections

**How many customers are inactive?**

Answer: 15

```sql
SELECT
  *
FROM
  customers
WHERE
  active = 0;
```

**How many movies did `customer_id` = 318 rent?**

Answer: 12

```sql
SELECT
  *
FROM
  rentals
WHERE
  customer_id = 318;
```

**How many payments have an amount `>=` 11.99?**

Answer: 10

```sql
SELECT
  *
FROM
  payments
WHERE
  amount >= 11.99;
```

**How many movies do not last more than 46 minutes (included)?**

Answer: 5

```sql
SELECT
  *
FROM
  films
WHERE
  length <= 46;
```