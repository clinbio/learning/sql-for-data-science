# Conclusion

Learning SQL is a long journey. It takes time and effort to master the language. However, it is a very rewarding
experience. SQL is a powerful tool that can be used to solve complex problems with ease. It is also a very popular
language that is used by many companies around the world. Therefore, learning SQL will open many doors for you in the
future. I hope this course has helped you get started on your journey to mastering SQL. Good luck!

## What's next?

Now that you have completed this course, you can continue your SQL journey by practicing your skills on a fun game [Lost at SQL](https://lost-at-sql.therobinlord.com/). You can also check out the [SQL Tutorial](https://www.sqltutorial.org/) website for more SQL resources.
While this tutorial has covered the basics of SQL, there is still a lot more to learn.


## References

- [MySQL for Developers](https://planetscale.com/learn/courses/mysql-for-developers)
- [SQL Tutorial - W3Schools](https://www.w3schools.com/sql/)
- [SQLite](https://www.sqlite.org/index.html)
- [LearnSQL.com](https://learnsql.com/)

## Credits

- [Dillenn TERUMALAI](mailto:dillenn.terumalai@sib.swiss)
- [Florent TASSY](mailto:florent.tassy@sib.swiss)

## License

[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)

## Acknowledgements

Clinical Bioinformatics Team, SIB Swiss Institute of Bioinformatics, Switzerland