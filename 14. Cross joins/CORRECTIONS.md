# Cross joins

### Padawan level... If we don't take the location into account, what are all the possible store-staff associations we can make ? Start with the staff and store IDs only

```sql
SELECT
  stores.store_id, -- we have to prefix the column here, because it exists in both tables!
  staff_id
FROM
  stores
  CROSS JOIN staffs;
```

### Jedi knight level... Refine the previous query by adding the staff first_name and last_name and the store address.

```sql
SELECT
  sto.store_id,
  addr.address,
  sta.staff_id,
  sta.first_name,
  sta.last_name
FROM
  stores sto
  CROSS JOIN staffs sta
  JOIN addresses addr ON sto.address_id = addr.address_id;
```