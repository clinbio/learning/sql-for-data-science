# Cross joins

## Exercices (~5 min)

### Padawan level... If we don't take the location into account, what are all the possible store-staff associations we can make ? Start with the staff and store IDs only

Answer:  

### Jedi knight level... Refine the previous query by adding the staff first_name and last_name and the store address.

Answer:  