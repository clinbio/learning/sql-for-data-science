# Cross joins

A cross join allows to return the Cartesian product of the tables to join. Joining tables A with 5 rows and table B with 10 rows, you will get 50 rows, whatever they contain. It actually creates all possible combinations of table rows.  
There are rather few use cases as it usually goes against the idea of **relational** databases, but it is good to know that it exists.

## Multiplication tables

We have two tables containing multipliers and multiplicators. We would like to combine them to provide a multiplication table. We can take an exemple with the to tables below:  

* Table multipliers

| multiplier |
| --- |
| 6 |

* Table multiplicators

| multiplicator |
| --- |
| 1 |
| 2 |
| 3 |
| 4 |
| 5 |
| 6 |
| 7 |
| 8 |
| 9 |
| 10 |

These tables can be create and filled with the code below:  

```sql
CREATE TABLE
  "multipliers" (multiplier INT NOT NULL, PRIMARY KEY (multiplier));
  
INSERT INTO
  "multipliers"
VALUES
  (6);

CREATE TABLE
  "multiplicators" (
    multiplicator INT NOT NULL,
    PRIMARY KEY (multiplicator)
  );
  
INSERT INTO
  "multiplicators"
VALUES
  (1),
  (2),
  (3),
  (4),
  (5),
  (6),
  (7),
  (8),
  (9),
  (10);

-- To delete the tables :
-- DROP TABLE "multiplicators";
-- DROP TABLE "multipliers";
```

We can use a cross join to create our multiplication table:  

```sql
 SELECT
    multiplier,
    'x',
    multiplicator,
    '=',
    (multiplier*multiplicator) AS product
 FROM 
    multipliers
 CROSS JOIN 
    multiplicators;
```

Which results in:  

| multiplier | &#039;x&#039; | multiplicator | &#039;=&#039; | product |
| ---------- | ------------- | ------------- | ------------- | ------- |
| 6          | x             | 1             | =             | 6       |
| 6          | x             | 2             | =             | 12      |
| 6          | x             | 3             | =             | 18      |
| 6          | x             | 4             | =             | 24      |
| 6          | x             | 5             | =             | 30      |
| 6          | x             | 6             | =             | 36      |
| 6          | x             | 7             | =             | 42      |
| 6          | x             | 8             | =             | 48      |
| 6          | x             | 9             | =             | 54      |
| 6          | x             | 10            | =             | 60      |

