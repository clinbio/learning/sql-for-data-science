/* Use plural table names */
ALTER TABLE actor RENAME TO actors;
ALTER TABLE address RENAME TO addresses;
ALTER TABLE category RENAME TO categories;
ALTER TABLE city RENAME TO cities;
ALTER TABLE country RENAME TO countries;
ALTER TABLE customer RENAME TO customers;
ALTER TABLE film RENAME TO films;
ALTER TABLE film_actor RENAME TO actor_film;
ALTER TABLE film_category RENAME TO category_film;
ALTER TABLE inventory RENAME TO inventories;
ALTER TABLE language RENAME TO languages;
ALTER TABLE payment RENAME TO payments;
ALTER TABLE rental RENAME TO rentals;
ALTER TABLE staff RENAME TO staffs;
ALTER TABLE store RENAME TO stores;

DROP TABLE film_text;

/* Make addresses.district nullable */
ALTER TABLE addresses RENAME COLUMN district TO district_old;
ALTER TABLE addresses ADD COLUMN district VARCHAR(20);
UPDATE addresses SET district = NULLIF(TRIM(district_old), '');
ALTER TABLE addresses DROP COLUMN district_old;

/* Add random phone numbers */
UPDATE addresses SET phone='+' || REPLACE(CAST(CEIL(ABS(RANDOM()) / POW(10, 8)) AS TEXT), '.0', '');
UPDATE addresses SET phone=SUBSTR(phone, 1, 3) || ' (0) ' || SUBSTR(phone, 4, 2) || '-' || SUBSTR(phone, 7, 2) || '-' || SUBSTR(phone, 9);