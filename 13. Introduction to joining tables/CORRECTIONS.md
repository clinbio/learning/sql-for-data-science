# Introduction to joining tables

## Corrections

### Create a listing of the staff members with their phone numbers.

```sql
SELECT 
    st.first_name, st.last_name, ad.phone
FROM 
    staffs st
JOIN addresses ad ON 
    st.address_id = ad.address_id;
```

### Create a listing of the films (title and release year) in which MARY KEITEL has played.

```sql
SELECT 
    f.title, f.release_year
FROM 
    films f
JOIN actor_film af ON 
    f.film_id = af.film_id
JOIN actors a ON
    a.actor_id = af.actor_id
WHERE
    a.first_name = 'MARY' AND
    a.last_name = 'KEITEL';
```

### How many times a film with MARY KEITEL has been rented?

```sql
SELECT 
    COUNT(*)
FROM 
    actors a
JOIN actor_film af ON 
    af.actor_id = a.actor_id
JOIN films f ON
    f.film_id = af.film_id
JOIN inventories i ON
    i.film_id = f.film_id
JOIN rentals r ON
    r.inventory_id = i.inventory_id
WHERE
    a.first_name = 'MARY' AND
    a.last_name = 'KEITEL';
```