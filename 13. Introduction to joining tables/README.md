# Introduction to joining tables

Relational databases allow to store data in a data model, on a structured and optimized way. A safe principle is to never store the same thing twice, but rather use relations (a.k.a. foreign keys) to refer to shared data pieces.  

However, when it comes to data display, reporting... it becomes necessary to put together pieces of data that are stored in different tables. It is where *joins* come to the rescue.

## A concrete example

### Table structures

I want to establish a listing of the staff with their postal addresses. However, my staff are recorded in `staffs` table, while the addresses are recorded in the `addresses`, `cities` and `countries` table.  

That makes senses on structure point of view : `addresses` and contains all addresses for staff, stores and customers. No need to repeat the same structures, and each entity that needs an address can get one by just having a foreign key to `address`.  

The same way, one city can have thounsands or more addresses, so `addresses` just need a foreign key to `cities`.  

### Joining two tables

A simple join looks like this :  

```sql
SELECT
  ad.address, cit.city
FROM
  addresses ad
JOIN cities cit ON
  ad.city_id = cit.city_id;
```

Here, we have used the "JOIN" keyword to explain the database which tables should be joint, and on which column.  

The table `addresses` is then joint on cities via their `city_id` columns.  

Notice the use of table *aliases*. Here, we alias addresses with "ad" and cities with "cit". Indeed, there are two columns named `city_id` here, so we have to specify which one to use when writing the query. Aliases are usually used in that case to keep the code shorter. It possible to write a join without aliases by keeping the full table names :  

```sql
SELECT
  addresses.address, cities.city
FROM
  addresses 
JOIN cities ON
  addresses.city_id = cities.city_id;
```

### Joining several tables

So if we want to get complete addresses, we then need to join tables together. This can be done as follows:

```sql
SELECT
	ad.address || 
  COALESCE(' ' || ad.address2, '') || 
  COALESCE(', ' || ad.district, '') || 
  ', ' || 
  COALESCE(ad.postal_code || ' ', '') || 
  cit.city || 
  ', ' || 
  cnt.country AS "Full address"
FROM
	addresses ad
JOIN cities cit ON
	ad.city_id = cit.city_id
JOIN countries cnt ON
	cit.country_id = cnt.country_id;
```

This will result in:  

| Full address                                                 |
|--------------------------------------------------------------|
| 47 MySakila Drive, Lethbridge, Canada                        |
| 28 MySQL Boulevard, Woodridge, Australia                     |
| 23 Workhaven Lane, Lethbridge, Canada                        |
| 1411 Lillydale Drive, Woodridge, Australia                   |
| 1913 Hanoi Way, 35200 Sasebo, Japan                          |
| 1121 Loja Avenue, 17886 San Bernardino, United States        |
| 692 Joliet Street, 83579 Athenai, Greece                     |
| 1566 Inegl Manor, 53561 Myingyan, Myanmar                    |
| 53 Idfu Parkway, 42399 Nantou, Taiwan                        |
| 1795 Santiago de Compostela Way, 18743 Laredo, United States |
| ...                                                          |

Several comments are usefull to understand this query :  

* The "||" operator is used for concatenating strings. Basically, `SELECT 'Hello' || ' ' || 'World';` results in : `Hello World`. However, any null variable among the concatened strins results in a fully empty string. `SELECT 'Hello' || NULL || 'World';` results in : `(NULL)`. So...
* ... we use the COALESCE function. Tihs function takes several values as parameter and returns the first non-null one. `SELECT COALESCE(NULL, 'Hello', 'World');` return `Hello`. It means that using `SELECT COALESCE(address2, '') FROM addresses;` will return empty strings instead of NULL, allowing our concatenation to work well.

### The listing and the secret for performant joins

Further on, if we want to get a listing with staff members and their addresses, we just need one more join :  

```sql
SELECT
    st.first_name || ' ' || st.last_name AS "Full name",
	ad.address || 
  COALESCE(' ' || ad.address2, '') || 
  COALESCE(', ' || ad.district, '') || 
  ', ' || 
  COALESCE(ad.postal_code || ' ', '') || 
  cit.city || 
  ', ' || 
  cnt.country AS "Full address"
FROM
    staffs st
JOIN addresses ad ON 
    ad.address_id = st.address_id
JOIN cities cit ON
	ad.city_id = cit.city_id
JOIN countries cnt ON
	cit.country_id = cnt.country_id;
```

Modern database engines are very powerful at making joins. There are some principles to respect to ensure optimal performance :  
* joins are more performant on numeric columns
* joins are more performant using indexed columns
* the table in the `FROM` clause should better be the one with less records (e.g. starting with `staffs` table) to limit the number of joins
* the syntax:
```sql
SELECT
	ad.address, cit.city
FROM
	addresses ad, cities cit
WHERE ad.city_id = cit.city_id;
```
is called an "implicit join" and is usually recognized. However, it is highly discouraged as it lacks some details for optimization and results in slower queries. Also it makes less clear that the query is actually joining tables.  

There are several types of joins addressing different use cases. In the next chapters, we are going to go through the different join types.
