# Introduction to joining tables

## Exercices (~5 min)

### Create a listing of the staff members with their phone numbers.

Answer:  

### Create a listing of the films (title and release year) in which MARY KEITEL has played.

Answer:  

### How many times a film with MARY KEITEL has been rented?

Answer:  