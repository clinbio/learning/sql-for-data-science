# What is SQL?

## Definition

**SQL** or Structured Query Language (pronounced as "sequel" or "S-Q-L") is a domain-specific language used to manage and
interact with relational databases. It is designed for creating, querying, updating, and managing data stored in a
structured format within a database.

![xkcd.png](xkcd.png)

## Declarative language

SQL is a declarative language, which means that it focuses on describing the desired outcome or result, rather than
specifying the step-by-step procedures to achieve that result. When you write an SQL query, you specify what data you
want to retrieve, update, or delete, but you do not provide detailed instructions on how the database should accomplish
the task. The database management system (DBMS) is responsible for determining the most efficient way to execute the
query.

## Usage

SQL allows users to perform various operations on data, such as:

- **Querying data:** `SELECT` statements are used to retrieve data from one or more tables based on specified criteria,
  and can include filtering, sorting, and aggregation functions.
- **Inserting data:** `INSERT` statement allows users to add one or multiple records to a table.
- **Updating data:** `UPDATE` statement modify existing record in a table.
- **Deleting data:** `DELETE` statement remove records from a table
- **Create/Modify structure of databases and tables:** Commands like `CREATE DATABASE`, `CREATE TABLE`, `ALTER TABLE` allows
  you to define or modify the structure of databases and tables.

As you can statements are made up of descriptive words which are very easy to understand.

## Valuable skill

SQL can be a valuable skill in the job market. Many companies store data in relational databases, and SQL knowledge is
often a required or preferred skill for data science positions.
See https://survey.stackoverflow.co/2022/#most-popular-technologies-language-prof.
