# 10. Aggregate functions: AVG, COUNT, MIN, MAX and SUM

## Corrections

### How many countries do we have in the database ? 

```sql
SELECT
    COUNT(*)
FROM
    countries;
```

|COUNT(*)|
|--------|
|109|

### Create a query that will allow reporting the number of payments, the average amount, the minimal and maximal value and the sum of all payments.

```sql
SELECT
    COUNT(*),
    AVG(amount),
    MIN(amount),
    MAX(amount),
    SUM(amount)
 FROM
 	payments;
```

|COUNT(*)|AVG(amount)|MIN(amount)|MAX(amount)|SUM(amount)|
|--------|-----------|-----------|-----------|-----------|
|16049|4.2006673312974065|0|11.99|67416.50999999208|

### Customize the report previously created by adding column aliases, rounding values to 2 decimals and execute it for the staff ID 1 only.

```sql
SELECT
    COUNT(*) AS "Number of payments for staff #1",
    ROUND(AVG(amount), 2) AS "Average rental amount for staff #1",
    MIN(amount) AS "Minimal payment amount for staff #1",
    MAX(amount) AS "Maximal payment amount for staff #1",
    ROUND(SUM(amount), 2) AS "Total payments amount for staff #1"
 FROM
 	payments
 WHERE
    staff_id = 1;
```

|Number of payments for staff #1|Average rental amount for staff #1|Minimal payment amount for staff #1|Maximal payment amount for staff #1|Total payments amount for staff #1|
|-------------------------------|----------------------------------|-----------------------------------|-----------------------------------|----------------------------------|
|8057|4.16|0|11.99|33489.47|
