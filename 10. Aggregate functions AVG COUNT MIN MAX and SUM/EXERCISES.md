# 9. Ordering your results with ORDER BY

## Exercices (~5 min)

### How many countries do we have in the database ? 

Answer:

### Create a query that will allow reporting the number of payments, the average, minimal and maximal amounts, and the sum of all payments.

Answer:

### Customize the report previously created by adding column aliases, rounding values to 2 decimals and execute it for the staff ID 1 only.

Answer: