# 10. Aggregate functions: AVG, COUNT, MIN, MAX and SUM

Aggregate functions allow grouping several rows into one. When using them, it is important to mix them with the correct columns. 

In this chapter, we will focus on the functions :  
* that are standard i.e. defined in SQL standard
* applicable to numeric columns

We will see [how these functions are implemented in SQLite](https://www.sqlite.org/lang_aggfunc.html). Please note that SQLite offers [many other non-standard functions](https://www.sqlite.org/lang_mathfunc.html).  

## AVG, the average of a column

The AVG function allows to get the average of a column. 

I want to see the average amount of a rental:

```sql
SELECT
    AVG(amount)
 FROM
    payments;
```

|AVG(amount)|
|-----------|
|4.2006673312974065|

Here, using aliases may make sense :  

```sql
SELECT
    AVG(amount) AS "Average rental amount"
 FROM
    payments;
```

|Average rental amount|
|---------------------|
|4.2006673312974065|

Since the AVG function aggregates row, it is not possible to mix non-aggregated columns and aggregated ones in one single query. For instance, this will give some unexpected result :  

```sql
SELECT
    payment_date,
    AVG(amount) AS "Average rental amount"
 FROM
    payments;
```

|payment_date|Average rental amount|
|------------|---------------------|
|2005-05-25 11:30:37.000|4.2006673312974065|

Why `2005-05-25 11:30:37.000`?!?? It does not make sense... because the query does not make sense.  

It is possible however to use several aggregated functions together as long as they produce the same number of lines :  

```sql
SELECT
    AVG(rental_duration) "Average rental duration",
    AVG(rental_rate) AS "Average rental rate",
    AVG(replacement_cost) AS "Average replacement cost",
    AVG(length) AS "Average film length"
 FROM
    films;
```

|Average retal duration|Average rental rate|Average replacement cost|Average film length|
|---------------------|-------------------|------------------------|-------------------|
|4.985|2.979999999999938|19.984000000000144|115.272|

### How does AVG function work with empty values ?

The best thing to do for further details is to look for the [official documentation](https://www.sqlite.org/lang_aggfunc.html) :  

    The avg() function returns the average value of all non-NULL X within a group. (...) The result of avg() is NULL if and only if there are no non-NULL inputs. 

## MIN, the lowest value of the column, and MAX, the higest one

MIN and MAX return, as their names suggest, the minimal and maximal values of a column.  

I want the lowest and highest payments of a column :  

```sql
SELECT
    MIN(amount) AS "Smallest payment amount",
    MAX(amount) AS "Highest payment amount"
 FROM
    payments;
```

|Smallest payment amount|Highest payment amount|
|-----------------------|----------------------|
|0|11.99|

## SUM, for summing all values of a column

I want the total amount of payments :  

```sql
SELECT
    SUM(amount) AS "Total payments amount"
 FROM
    payments;
```

|Total payments amount|
|---------------------|
|67416.50999999208|

## Tips : limit the number of decimal values

This can be achived with the ROUND function :  

```sql
SELECT
    ROUND(AVG(amount), 2) AS "Average rental amount",
    ROUND(SUM(amount), 2) AS "Total payments amount"
 FROM
    payments;
```

|Average rental amount|Total payments amount|
|---------------------|---------------------|
|4.2|67416.51|

Notice in the average column that the non-significant '0' are trimmed.

## COUNT, the number of values in the column

The COUNT function returns the number of row in a column. It can be used on non-numeric columns as well as on several columns :  

```sql
SELECT 
    COUNT(*) as "Number of payments"
 FROM 
    payments;
```

|Number of payments|
|------------------|
|16049|
