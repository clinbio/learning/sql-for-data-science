# Introduction to joining tables

### Which swiss cities are present in the database?

```sql
SELECT
  *
FROM
  cities cit
  INNER JOIN countries ctr ON cit.country_id = ctr.country_id
WHERE
  country = 'Switzerland';
```

### List the customers (ID, first name, last name, email and city) whose address is in Switzerland.

```sql
SELECT
  cust.customer_id,
  cust.first_name,
  cust.last_name,
  cust.email,
  cit.city
FROM
  customers cust
  INNER JOIN addresses addr ON cust.address_id = addr.address_id
  INNER JOIN cities cit ON addr.city_id = cit.city_id
  INNER JOIN countries ctr ON cit.country_id = ctr.country_id
WHERE
  country = 'Switzerland';
```

### List the customers (ID, first name, last name, email and city) whose address is in Switzerland and their respective number or rentals.

```sql
SELECT
  cust.customer_id,
  cust.first_name,
  cust.last_name,
  cust.email,
  cit.city,
  COUNT(rnt.rental_id) AS "Nb of rentals"
FROM
  customers cust
  INNER JOIN addresses addr ON cust.address_id = addr.address_id
  INNER JOIN cities cit ON addr.city_id = cit.city_id
  INNER JOIN countries ctr ON cit.country_id = ctr.country_id
  INNER JOIN rentals rnt ON cust.customer_id = rnt.customer_id
WHERE
  country = 'Switzerland'
GROUP BY 
  cust.customer_id, 
  cust.first_name, 
  cust.last_name,
  a.address;
```

