# Inner joins

An inner join allow to retrieve the intersection of joint tables based on some provided join columns.  

Considering these tables:  

* users

| user_id | username |
| --- | --- |
| 1 | jdoe |
| 2 | userY |
| 3 | fdupont |

* contacts

| contact_id | contact_info | user_id |
| --- | --- | --- |
| 1 | jdoe@mail.com | 1 |
| 2 | +41 71 345 32 65 | 1 |
| 3 | fdupont@mail.com | 3 |
| 4 | +41 72 987 54 43 | 4 |

... that can be created with the code below :  

```sql
CREATE TABLE
  "users" (
    user_id INT NOT NULL,
    username TEXT,
    PRIMARY KEY (user_id)
  );

INSERT INTO
  "users"
VALUES
  (1, 'jdoe'),
  (2, 'userY'),
  (3, 'fdupont');

CREATE TABLE
  "contacts" (
    contact_id INT NOT NULL,
    contact_info TEXT,
    user_id INT,
    PRIMARY KEY (contact_id)
  );

INSERT INTO
  "contacts"
VALUES
  (1, 'jdoe@mail.com', 1),
  (2, '+41 71 345 32 65', 1),
  (3, 'fdupont@mail.com', 3),
  (4, 'someone@mail.com', 4);

-- To delete the tables :
-- DROP TABLE "contacts";
-- DROP TABLE "users";
```

If we want to associate the users to their contact informations, we can use the `user_id` column. The query will look like :  

```sql
SELECT
    *
FROM
    users u
INNER JOIN contacts c ON u.user_id = c.user_id;
```

The result will be :  

| user_id | username | contact_id | contact_info     |
| ------- | -------- | ---------- | ---------------- |
| 1       | jdoe     | 1          | jdoe@mail.com    |
| 1       | jdoe     | 2          | +41 71 345 32 65 |
| 3       | fdupont  | 3          | fdupont@mail.com |

As you can notice, the user #2 "userY" is not in the result, because the table `contacts` has no row with `user_id` = 2. The same way, contact #4 "someone@mail.com" is not in the results, because there is no row with `user_id` = 4 in table `users`.  

So the INNER JOIN query keep *only the rows for which join criteria are present in both joint table*.  

Also, using "JOIN" alone is usually equivalent to using "INNER JOIN". But it is more explicit to always specify.