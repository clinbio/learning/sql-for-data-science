# Data models: structure and content

## Definitions

### Database

    In computing, a database is an organized collection of data stored and accessed electronically.
    <cite> https://en.wikipedia.org/wiki/Database </cite>

A database aims at storing data on an efficient and secure way. It also provides some features such as user authentication, concurrence access, **query language**...

## Relational database 

There are several database paradigms :
* Relational
* Key-value
* Graph
* Document
* ...

https://fireship.io/lessons/top-seven-database-paradigms/
  
A relational database is a database that stores data based on the **relational model** established by E. F. Codd in 1970.  

The relational model principle is to store data in **tables**. A table consists in **rows** and **columns**. **Relations** and **constraints** are defined between different tables.

### Data model

    A data model is an abstract model that organizes elements of data and standardizes how they relate to one another and to the properties of **real-world entities**.
    <cite> https://en.wikipedia.org/wiki/Data_model </cite>

The relational data model covers :
* Structure (table, columns)
* Relations

A data model does **not** covers :
* Data (rows)
* Flow

A data model can be represented by an **entity-relationship diagram** (ERD). The ERD follows conventions to represent a data relational data model.  

## Example of an entity-relationship diagram

![Example of an entity-relationship diagram with three tables](./example_ERD.svg)

## Data model elements

* Tables
  * Columns
  * Types
  * (Length)
* Relations
  * Cardinality
* Constraints
  * Key
  * Unicity
  * Nullability



