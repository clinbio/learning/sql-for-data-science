# Presentation of our used case

The **Sakila** schema is a sample database schema provided by MySQL for educational and demonstration purposes. It
represents a fictional video rental store called **"Sakila"** and showcases various database features, design patterns, and
best practices. The **Sakila** schema consists of several tables, which are organized into entities related to the video
rental business. Here's a brief overview of the main tables in the schema:

- **actors**: Stores information about the actors, including first and last name. Columns: `actor_id`, `first_name`, `last_name`, `last_update`

- **films**: Contains details about the films, such as title, description, release year, language, rental duration, rental
rate, length, and replacement cost. Columns: `film_id`, `title`, `description`, `release_year`, `language_id`, `original_language_id`, `rental_duration`, `rental_rate`,
  `length`, `replacement_cost`, `rating`, `special_features`, `last_update`

- **film_actor**: Represents the relationship between films and actors, serving as a junction table. Columns: `actor_id`, `film_id`, `last_update`

- **film_category**: Associates films with their respective categories, also a junction table. Columns: `film_id`, `category_id`, `last_update`

- **category**: Contains information about film categories, such as name. Columns: `category_id`, `name`, `last_update`

- **language**: Stores the languages available for films. Columns: `language_id`, `name`, `last_update`

- **inventory**: Holds information about the inventory of films in the rental store. Columns: `inventory_id`, `film_id`, `store_id`, `last_update`

- **store**: Contains details about each store location, including the address and manager. Columns: `store_id`, `manager_staff_id`, `address_id`, `last_update`

- **staff**: Holds information about the staff members, including name, address, and other contact information. Columns: `staff_id`, `first_name`, `last_name`, `address_id`, `email`, `store_id`, `active`, `username`, `password`, `last_update`, `picture`

- **customer**: Stores information about customers, such as name, address, and contact information. Columns: `customer_id`, `store_id`, `first_name`, `last_name`, `email`, `address_id`, `active`, `create_date`, `last_update`

- **address**: Contains address information for customers, staff, and stores. Columns: `address_id`, `address`, `address2`, `district`, `city_id`, `postal_code`, `phone`, `last_update`

- **city**: Stores city names and their corresponding country. Columns: `city_id`, `city`, `country_id`, `last_update`

- **country**: Contains country names. Columns: `country_id`, `country`, `last_update`

- **rental**: Holds information about rental transactions, such as rental date, return date, and customer/staff details. Columns: `rental_id`, `rental_date`, `inventory_id`, `customer_id`, `return_date`, `staff_id`, `last_update`

- **payment**: Stores information about payments made by customers, including amount, date, and rental information. Columns: `payment_id`, `customer_id`, `staff_id`, `rental_id`, `amount`, `payment_date`

These tables are connected through various relationships, such as one-to-one, one-to-many, and many-to-many, to
represent the complex data structure of a video rental business.

![image](schema.png)