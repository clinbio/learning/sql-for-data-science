# 9. Ordering your results with ORDER BY

With relational databases, we should **NEVER** assume that query result is sorted if it is not explicitely set this way. It is also true for data in table: we should **NEVER** assume that :
* the primary key is in ascending order
* or the next primary key when creating a row will be the current highest one + 1
* or, provided that the primary key with value `n` exists, then the primary key with value `n-1` exists too.

Relational databases are not built on these assumptions, and you are at risk of doing huge and unoticeable mistakes if you do so. 

So, when you have several rows as a result of a SQL query, you may wish to sort your results per one or several columns.

This is doable with the "ORDER BY" instruction, located after the "WHERE" clause.  

## Sort on one column

### Number

I want to see the movies sorted by length:

```sql
SELECT
	film_id, title, length
 FROM
 	films
 ORDER BY
 	length;
```

The films are sorted by length, the first one being the shortest, and the last one being the longest.

### Date

I want to sort rentals by return date:

```sql
SELECT
    *
FROM
    rentals
ORDER BY
    return_date;
```

⚠️ Notice that null columns come first !  
Otherwise, the rentals are sorted with the oldest return first, and most recent return last.  

### Text

I want to see the actors ordered by their last names:

```sql
SELECT
    *
FROM
    actors
ORDER BY
    last_name;
```

The actors are sorted by they last name alphabetical order.

## Ascending or descending

As we have seen, if we simply use ORDER BY, we get something sorted in **ascending** order. We can explicitely set ordering with "ASC" and "DESC" keywords.  
I want to see all payments, from highest to smallest amount:  

```sql
SELECT
    *
FROM
    payments
ORDER BY
    amount DESC;
```

## Sort on multiple columns

We can also sort query results on multiple columns. In that case, the order you give columns after the ORDER BY keyword determines the way the results with be sorted.  
You can specify for each column if it is ascending or descending.

I want to get the films by decreasing rental rate, and by ascending length :  

```sql
SELECT
    *
FROM
    films
ORDER BY
    rental_rate DESC, 
    length ASC;
```
