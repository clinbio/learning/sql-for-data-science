# 9. Ordering your results with ORDER BY

## Corrections

### What is the length of the longest film ?

```sql
SELECT
    length
FROM
    films
ORDER BY
    length DESC
LIMIT 1;
```

### Which film has the highest rental rate, the highest replacement cost but the lowest rental duration?

Answer: WEST LION

```sql
SELECT
    *
FROM
    films
ORDER BY
    rental_rate DESC,
    replacement_cost DESC,
    rental_duration ASC;
```

### Select the films whose title starts with "V", ends with "Y" or , sort them by desceding rental rate and replacement cost. What is the first film title ?

```sql
SELECT
    *
FROM
    films
WHERE
    title LIKE 'V%Y' 
ORDER BY
    rental_rate DESC,
    replacement_cost DESC;
```
