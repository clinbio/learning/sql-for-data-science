# 9. Ordering your results with ORDER BY

## Exercices (~5 min)

### What is the length of the longest film ?

Answer:

### Which film has the highest rental rate, the highest replacement cost but the lowest rental duration?

Answer

### Select the films whose title starts with "V", ends with "Y", sort them by descending rental rate and replacement cost. What is the first film title ?

Answer: