SELECT
	COUNT(*)
FROM
	films
WHERE
	rental_rate < (
    SELECT
    	AVG(rental_rate)
 		FROM
    	films
    )
AND
	rental_duration = (
    SELECT
    	MAX(rental_duration)
    FROM
    	films
    );




