SELECT
  COUNT(*)
FROM
  (
    SELECT
      first_name
    FROM
      actors EXCEPT
    SELECT
      first_name
    FROM
      customers
  );