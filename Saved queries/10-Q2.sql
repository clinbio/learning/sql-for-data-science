SELECT
	COUNT(*) AS 'Nb of payments',
  AVG(amount) AS 'Average amount',
  MIN(amount) AS 'Minimum amount',
  MAX(amount) AS 'Maximum amount',
  SUM(amount) AS 'Sum of all payments'
FROM
	payments;









