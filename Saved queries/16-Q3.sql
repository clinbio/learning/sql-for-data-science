SELECT
  cit.city_id,
  cit.city,
  cnt.country_id,
  cnt.country,
  COUNT(cust.customer_id) AS "Nb of customers"
FROM
  countries cnt
  LEFT JOIN cities cit ON cnt.country_id = cit.country_id
  LEFT JOIN addresses addr ON cit.city_id = addr.city_id
  LEFT JOIN customers cust ON addr.address_id = cust.address_id
GROUP BY
  cit.city_id
ORDER BY
  COUNT(cust.customer_id) ASC;