SELECT
  COUNT(r.rental_id) AS 'Total of rentals'
FROM
	films f
JOIN
	actor_film af ON af.film_id = f.film_id
JOIN
	actors a ON a.actor_id = af.actor_id
JOIN
	inventories i ON i.film_id = f.film_id
JOIN
	rentals r ON r.inventory_id = i.inventory_id
WHERE
	a.first_name = 'MARY'
AND
	a.last_name = 'KEITEL';