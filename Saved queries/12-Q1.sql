SELECT
	city
FROM
	cities
WHERE
	country_id IN (
    SELECT
    	country_id
  	FROM
    	countries
    WHERE
    	country LIKE 'L%'
    );