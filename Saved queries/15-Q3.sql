SELECT
	cu.customer_id,
  cu.first_name,
  cu.last_name,
  a.address,
  ci.city,
  COUNT(r.rental_id) AS "Nb of rentals"
FROM
	cities ci
INNER JOIN
	countries co ON ci.country_id = co.country_id
INNER JOIN
	addresses a ON a.city_id = ci.city_id
INNER JOIN
	customers cu ON cu.address_id = a.address_id
INNER JOIN
	rentals r on r.customer_id = cu.customer_id
WHERE
	co.country = 'Switzerland'
GROUP BY
	cu.customer_id,
  cu.first_name,
  cu.last_name,
  a.address,
  ci.city;









