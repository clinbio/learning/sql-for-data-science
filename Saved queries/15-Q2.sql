SELECT
	cu.customer_id,
  cu.first_name,
  cu.last_name,
  a.address,
  ci.city
FROM
	cities ci
INNER JOIN
	countries co ON ci.country_id = co.country_id
INNER JOIN
	addresses a ON a.city_id = ci.city_id
INNER JOIN
	customers cu ON cu.address_id = a.address_id
WHERE
	co.country = 'Switzerland';









