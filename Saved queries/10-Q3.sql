SELECT
	COUNT(*) AS 'Nb of payments',
  ROUND(AVG(amount), 2) AS 'Average amount',
  ROUND(MIN(amount), 2) AS 'Minimum amount',
  ROUND(MAX(amount), 2) AS 'Maximum amount',
  ROUND(SUM(amount), 2) AS 'Sum of all payments'
FROM
	payments
WHERE
	staff_id = 1;