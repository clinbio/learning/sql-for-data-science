SELECT
  COUNT(*)
FROM
  (
    SELECT
      customer_id,
      SUM(amount) AS 'total'
    FROM
      payments
    GROUP BY
      customer_id
    HAVING
      SUM(amount) > (
        SELECT
          AVG(sum)
        FROM
          (
            SELECT
              SUM(amount) as 'sum'
            FROM
              payments
            GROUP BY
              customer_id
          )
      )
  );









