SELECT
	s.staff_id,
  s.first_name,
  s.last_name,
  a.phone
FROM
	staffs s
JOIN
	addresses a ON a.address_id = s.address_id;
