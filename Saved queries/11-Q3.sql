SELECT
	customer_id,
  COUNT(*) AS 'Total rentals',
  ROUND(AVG(amount), 2) AS 'Average payment',
  ROUND(SUM(amount), 2) AS 'Total payments'
FROM
	payments
GROUP BY
	customer_id
ORDER BY
	SUM(amount) DESC
LIMIT
	5;









