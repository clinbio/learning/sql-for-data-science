SELECT
	c.city_id,
  c.city,
  co.country_id,
  co.country
FROM
	addresses a
RIGHT JOIN cities c
	ON c.city_id = a.city_id
INNER JOIN countries co
	ON c.country_id = co.country_id
WHERE
	a.city_id IS null;









