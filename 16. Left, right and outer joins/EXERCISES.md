# Left, right and outer joins

## Exercices (~5 min)

### Using a left join, which cities (city_id, city) are not referenced by any address?

Answer:  

### Using a right join, rewrite the same query and add the country information (country_id, country). What are the countries?

Answer:  

### We want to identify the cities (city_id, city, country_id, country) where we have the least customers. What are those cities?

Answer:  