# Left, right and outer joins

### Using a left join, which cities (city_id, city) are not referenced by any address?

```sql
SELECT
  cit.city_id,
  cit.city
FROM
  cities cit
  LEFT JOIN addresses addr ON cit.city_id = addr.city_id
WHERE
  addr.city_id IS NULL;
```

### Using a right join, rewrite the same query and add the country information (country_id, country). What are the countries?

```sql
SELECT
  cit.city_id,
  cit.city,
  cnt.country_id,
  cnt.country
FROM
  addresses addr
  RIGHT JOIN cities cit ON addr.city_id = cit.city_id
  INNER JOIN countries cnt ON cit.country_id = cnt.country_id
WHERE
  addr.city_id IS NULL;
```

### We want to identify the cities (city_id, city, country_id, country) where we have the least customers. What are those cities?

```sql
SELECT
  cit.city_id,
  cit.city,
  cnt.country_id,
  cnt.country,
  COUNT(cust.customer_id) AS "Nb of customers"
FROM
  countries cnt
  LEFT JOIN cities cit ON cnt.country_id = cit.country_id
  LEFT JOIN addresses addr ON cit.city_id = addr.city_id
  LEFT JOIN customers cust ON addr.address_id = cust.address_id
GROUP BY
  cit.city_id
ORDER BY
  COUNT(cust.customer_id) ASC;
```