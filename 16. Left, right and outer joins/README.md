# Left, right and outer joins

Left and right joins are a way to join table while keeping all records on one of the table. They are also call left outer join and right outer join.  

The "left" table is to be understood as the table in the `FROM` clause or the first one put in the code. The right is then the second one that comes in the code.  

Let's take the examples of previous chapter again:

* users

| user_id | username |
| --- | --- |
| 1 | jdoe |
| 2 | userY |
| 3 | fdupont |

* contacts

| contact_id | contact_info | user_id |
| --- | --- | --- |
| 1 | jdoe@mail.com | 1 |
| 2 | +41 71 345 32 65 | 1 |
| 3 | fdupont@mail.com | 3 |
| 4 | +41 72 987 54 43 | 3 |

The following query :

```sql
SELECT
  *
FROM 
  users u
LEFT JOIN contacts c ON u.user_id = c.user_id;
```

...will show the "userY" user, because the left table is `users`, and we keep all records of the left table:  

|user_id|username|contact_id|contact_info|user_id|
|-------|--------|----------|------------|-------|
|1|jdoe|1|jdoe@mail.com|1|
|1|jdoe|2|+41 71 345 32 65|1|
||**userY**||||
|3|fdupont|3|fdupont@mail.com|3|

The same way, if we right join the table, we will keep the contact "someone@mail.com" that had no associated user:  

```sql
SELECT
  *
FROM
  users u
RIGHT JOIN contacts c ON u.user_id = c.user_id;
```

...results in:  

|contact_id|contact_info|user_id|user_id|username|
|----------|------------|-------|-------|--------|
|1|jdoe@mail.com|1|1|jdoe|
|2|+41 71 345 32 65|1|1|jdoe|
|3|fdupont@mail.com|3|3|fdupont|
|**4**|**someone@mail.com**|**4**|||

What if you wish to keep all the records of *both* tables? Here comes the `full join` syntax:  

```sql
SELECT
  *
FROM
  users u
FULL JOIN contacts c ON u.user_id = c.user_id;
```

...that results in:

| user_id | username | contact_id | contact_info     |
| ------- | -------- | ---------- | ---------------- |
| 1       | jdoe     | 1          | jdoe@mail.com    |
| 1       | jdoe     | 2          | +41 71 345 32 65 |
|         | **userY**    |            |                  |
| 3       | fdupont  | 3          | fdupont@mail.com |
| **4**       |          | **4**          | **someone@mail.com** |

Notice that the user_id of "userY" does not appear here. That's because the query results displays the columns with the same name only once, and they keep the value of the latest table, here `contacts`. We could force the display by using:  

```sql
SELECT
  users.user_id,
  username, 
  contact_id, 
  contact_info
FROM
  contacts
  FULL JOIN users ON users.user_id = contacts.user_id;
```

... but in that case, the user_id of "someone@mail.com" disappears. If we want to have all of them, the COALESCE function used on the join columns can help us:

```sql
SELECT
  COALESCE(u.user_id, c.user_id),
  username, 
  contact_id, 
  contact_info
FROM
  contacts c
  FULL JOIN users u ON u.user_id = c.user_id;
```

| COALESCE(users.user_id, contacts.user_id) | username | contact_id | contact_info     |
| ----------------------------------------- | -------- | ---------- | ---------------- |
| 1                                         | jdoe     | 1          | jdoe@mail.com    |
| 1                                         | jdoe     | 2          | +41 71 345 32 65 |
| 3                                         | fdupont  | 3          | fdupont@mail.com |
| **4**                                         |          | **4**          | **someone@mail.com** |
| **2**                                         | **userY**    |            |                  |
