# 19. Views

In the previous lesson, we learned how to use subqueries to create more complex queries. In this lesson, we will learn
how to use views to simplify complex queries.

## What is a view?

A view is a virtual table that is created from the result set of a query. It is a very powerful feature of SQL that can
be used to simplify complex queries. Views are often used to hide the complexity of a query from the user.

```sql
CREATE VIEW view_name AS
SELECT
  column_name(s)
FROM
    table_name
WHERE
    condition;
```

Here's an example of a view:

```sql
CREATE VIEW active_customers AS
SELECT
  first_name,
  last_name,
  email
FROM
    customers
WHERE
    active = 1;
```

This view retrieves the first_name, last_name, and email of all active customers from the customers table. It can be
used like a table in a query:

```sql
SELECT
  *
FROM
    active_customers;
```

## Why use views?

Views are very useful for simplifying complex queries. They can also be used to restrict access to sensitive data. For
example, you can create a view that only shows the
first_name, last_name, and email of all active customers from the customers table. This view can then be used in place
of the customers table in queries where you only need to retrieve the first_name, last_name, and email of active
customers.

## Updating views

When dealing with views, it is important to note that they are not updated automatically when the underlying tables are
updated. Therefore, you must update the views manually if you want them to reflect the changes made to the underlying
tables.

Views can be updated using the `CREATE OR REPLACE VIEW` statement. This statement allows you to update the definition of
a view without having to drop and recreate it.

```sql
CREATE OR REPLACE VIEW view_name AS
SELECT
  column_name(s)
FROM
    table_name
WHERE
    condition;
```

Here's an example of updating a view:

```sql
CREATE OR REPLACE VIEW active_customers AS
SELECT
  customer_id,
  first_name,
  last_name,
  email
FROM
    customers
WHERE
    active = 1;
```

## Dropping views

Views can be dropped using the `DROP VIEW` statement. This statement allows you to drop a view from the database.

```sql
DROP VIEW view_name;
```

Here's an example of dropping a view:

```sql
DROP VIEW active_customers;
```