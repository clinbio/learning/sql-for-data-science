# SQL for data science

## Table of Contents

### Module 1

* What is SQL?
* Data models: structure and content
* Datatypes in SQL
* Presentation of our used case
* Retrieve data with SELECT
* Filtering with SQL
* Limiting your results with LIMIT
* Advanced filtering: IN, OR, AND and NOT

### Module 2

* Wildcards
* Ordering your results with ORDER BY
* Aggregate functions: AVG, COUNT, MIN, MAX and SUM
* Grouping your results with GROUP BY

### Module 3

* Subqueries
* Introduction to joining tables
* Cross joins
* Inner joins
* Left, right and outer joins

### Module 4

* Set operations with UNION, INTERSECT and EXCEPT
* Date and Time
* Views
* Conclusion

## What is this tutorial about?

This tutorial is about SQL, a programming language designed to manage data stored in relational databases. It is a very
powerful tool used by a wide range of companies, from small startups to large multinationals. SQL is used to store,
manipulate and retrieve data stored in relational databases.

## Who is this tutorial for?

This tutorial is for anyone who wants to learn SQL. It is designed for beginners. No prior knowledge of SQL is required.

## What will you learn during this tutorial?

During this tutorial, you will learn the basics of SQL. You will learn how to retrieve data from a database using the
SQL language. You will also learn how to filter, order and group your results. You will learn how to join tables
together. You will also learn how to use set operations. Finally, you will learn how to use dates and times in SQL.

## What will you not learn during this tutorial?

This tutorial is not about database design. It is not about how to create a database, a table or a view. It is also not
about inserting, updating or deleting data.

## Which database management system will we use during this tutorial?

We will use SQLite, a relational database management system. It is a very popular database management system. It is also
very easy to install and use. It is also very easy to use with Python.



