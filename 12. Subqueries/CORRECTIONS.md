# Subqueries

## Corrections

**Which cities are located in a country from which the name starts with an 'L'?**

Answer:

| city       | country_id |
|------------|------------|
| Daugavpils | 54         |
| Liepaja    | 54         |
| Vaduz      | 55         |
| Vilnius    | 56         |

```sql
SELECT
  city, country_id
FROM  
  cities
WHERE
  country_id IN(
    SELECT
      country_id FROM countries
    WHERE
      country LIKE 'L%'
  );
```

**How many films have a rental rate lower than the average rental_rate and a maximal rental_duration?**

Answer: 59

```sql
SELECT
  COUNT(*)
FROM
  films fl
WHERE
  fl.rental_rate < (
    SELECT
      AVG(f.rental_rate)
    FROM
      films f
  )
  AND fl.rental_duration = (
    SELECT
      MAX(f.rental_duration)
    FROM
      films f
  );
```

**How many customers have a total payment greater than the average total payment?**

Answer: 285

```sql
SELECT
  COUNT(*)
FROM
  (
    SELECT
      p.customer_id,
      SUM(p.amount)
    FROM
      payments p
    GROUP BY
      p.customer_id
    HAVING
      SUM(p.amount) > (
        SELECT
          AVG(sum)
        FROM
          (
            SELECT
              SUM(pay.amount) AS 'sum'
            FROM
              payments pay
            GROUP BY
              pay.customer_id
          )
      )
    ORDER BY
      SUM(p.amount) DESC
  );
```
