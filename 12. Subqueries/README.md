# 12. Subqueries

The subquery is a query nested inside another query. It is a very powerful feature of SQL that allows you to combine
criteria and logic to produce more complex queries.

Subqueries can be used in the `WHERE` clause to filter data based on the results of the subquery. The subquery will be
run first and the results it returns will be used by the main query to filter the results.

```sql
SELECT 
  column_name(s)
FROM 
  table_name
WHERE 
  column_name IN (
    SELECT 
      column_name 
    FROM 
      table_name 
    WHERE 
      condition
  );
```

Here's an example of a subquery in the `WHERE` clause:

```sql
SELECT
  first_name,
  last_name
FROM
  customers
WHERE
  customer_id IN (
    SELECT
      customer_id
    FROM
      payments
    WHERE
      amount > 11
  );
```

Results:

| first_name | last_name |
|------------|-----------|
| KAREN      | JACKSON   |
| VICTORIA   | GIBSON    |
| VANESSA    | SIMS      |
| ALMA       | AUSTIN    |
| ROSEMARY   | SCHMIDT   |
| TANYA      | GILBERT   |
| RICHARD    | MCCRARY   |
| NICHOLAS   | BARFIELD  |
| KENT       | ARSENAULT |
| TERRANCE   | ROUSH     |

In this example, the subquery retrieves all customer_id values from the payments table where the amount is greater than
eleven. Then, the main query uses the results of the subquery to filter the customers table and retrieve the first_name
and last_name of the customers whose customer_id is in the results of the subquery.

As you can see, the subquery is enclosed in parentheses and is placed inside the `IN` operator. The `IN` operator is
used to check if a value is in a list of values. Therefore, the subquery must return a list of values for the `IN`
operator to check against.

It is important to note that the subquery must return a single compatible column. If the subquery returns multiple
columns, the main query will throw an error.

Finally, it is interesting to note that subqueries can also be used in the `FROM` clause to create a derived table or in
the `SELECT` clause to retrieve a single value.