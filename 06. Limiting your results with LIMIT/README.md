# Limiting your results with LIMIT

## The SQL LIMIT statement

The `LIMIT` statement is used to limit the number of results returned in a SQL statement. It is used in conjunction with
the `SELECT` statement to fetch a limited number of records, based on a specific `criteria` that is applied to the query
using `WHERE` clause.

```sql
SELECT
  *
FROM
    payments
WHERE
    amount > 11
LIMIT 
    5;
```

Results:

| payment_id | customer_id | staff_id | rental_id | amount | payment_date            | last_update         |
|------------|-------------|----------|-----------|--------|-------------------------|---------------------|
| 342        | 13          | 2        | 8831      | 11.99  | 2005-07-29 22:37:41.000 | 2020-12-23 07:19:14 |
| 3146       | 116         | 2        | 14763     | 11.99  | 2005-08-21 23:34:00.000 | 2020-12-23 07:19:54 |
| 5280       | 195         | 2        | 16040     | 11.99  | 2005-08-23 22:19:33.000 | 2020-12-23 07:20:25 |
| 5281       | 196         | 2        | 106       | 11.99  | 2005-05-25 18:18:19.000 | 2020-12-23 07:20:25 |
| 5550       | 204         | 2        | 15415     | 11.99  | 2005-08-22 23:48:56.000 | 2020-12-23 07:20:28 |

In this example, the `LIMIT` clause limits the number of rows returned to 5. The query retrieves all the columns for all
payments with an amount greater than `11`.