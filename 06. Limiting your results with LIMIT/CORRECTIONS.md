# Limiting your results with LIMIT

## Corrections

**Which query allows to retrieve only 10 films?**

- [ ] SELECT
	*
FROM
	films
ONLY
	10;


- [ ] SELECT
	*
FROM
	films
WHERE
	film_id <= 10;


- [X] SELECT
	*
FROM
	films
LIMIT
	10;