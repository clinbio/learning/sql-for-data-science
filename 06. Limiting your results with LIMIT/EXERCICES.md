# Limiting your results with LIMIT

## Exercises (~5 min)

**Which query allows to retrieve only 10 films?**

- [ ] SELECT
	*
FROM
	films
ONLY
	10;


- [ ] SELECT
	*
FROM
	films
WHERE
	film_id <= 10;


- [ ] SELECT
	*
FROM
	films
LIMIT
	10;