# Wildcards

## `LIKE` operator

The LIKE operator in SQL is used to search for a specified pattern in a column. It is often used in a WHERE clause to
filter rows based on partial or approximate string matches, rather than exact matches. The LIKE operator is commonly
used with wildcards to define the search pattern, with the percent sign (%) and underscore (_) being the most frequently
used wildcards.

Here's a brief explanation of the LIKE operator:

- **Percent sign** (`%`): Represents zero, one, or multiple characters in a search pattern.
- **Underscore** (`_`): Represents a single character in a search pattern.

The syntax for using the LIKE operator is:

```sql
SELECT
  column_name(s)
FROM
  table_name
WHERE
  column_name LIKE pattern;
```

Example:

```sql
SELECT
  *
FROM
  actors
WHERE
  first_name LIKE 'Z%';
```

This query retrieves all records from the actors table the first_name starts with the letter `Z `.

| actor_id | first_name | last_name | last_update         |
|----------|------------|-----------|---------------------|
| 11       | ZERO       | CAGE      | 2020-12-23 07:12:29 |

Another example:

```sql
SELECT
  *
FROM
  actors
WHERE
  first_name LIKE '__';
```

This query retrieves all records from the actors table where the first_name is two characters long and each character
can be any single character possible.

| actor_id | first_name | last_name | last_update         |
|----------|------------|-----------|---------------------|
| 3        | ED         | CHASE     | 2020-12-23 07:12:29 |
| 136      | ED         | MANSFIELD | 2020-12-23 07:12:30 |
| 165      | AL         | GARLAND   | 2020-12-23 07:12:31 |
| 179      | ED         | GUINESS   | 2020-12-23 07:12:31 |