# Advanced filtering: IN, OR, AND and NOT

## Corrections

**Which actor has a his last_name ending with Z?**

Answer: RALPH CRUZ

```sql
SELECT
  *
FROM
  actors
WHERE
  last_name LIKE '%Z';
```

**How many actors have a first_name of exactly 9 letters?**

Answer: 4

```sql
SELECT
  COUNT(*)
FROM
  actors
WHERE
  first_name LIKE '_________';
```