# 18. Date and Time

## Introduction

Interestingly, DATE and DATETIME are stored as numbers. The date is stored as a number of days since the epoch, and the
time is stored as a fraction of a day. This is why you can do math on dates and times, and why dates and times can be
sorted numerically.

## Date

Filtering by date is a common task in data analysis and is well handled by SQL. You can filter by date using the
classical `WHERE` clause with an operator like `=`, `>`, `<`, etc. You can also use the `BETWEEN` operator to filter by
a range of dates. Finally, you can use the `DATE()` function to extract the date part of a timestamp and DATETIME() to
extract the date and time part of a timestamp.

### `WHERE` clause

When combining `WHERE` with a date column, you can use the following operators: `=`, `>`, `<`, `>=`, `<=`, `<>`, `!=`.

Example:

```sql
SELECT
  *
FROM
    payments
WHERE
    DATE(payment_date) = '2005-05-24';
```

This query retrieves all records from the payments table where the payment_date is equal to February 15, 2007. Note that
we use the `DATE()` function to extract the date part of the payment_date column as it is a timestamp.

### `BETWEEN` operator

The `BETWEEN` operator is used to filter data based on a range of values. It is often used in a `WHERE` clause to filter
the results of a query by a range of values. The syntax for using the `BETWEEN` operator is:

```sql
SELECT
  column_name(s)
FROM
    table_name
WHERE
    column_name BETWEEN value1 AND value2;
```

Example:

```sql
SELECT
  *
FROM
    payments
WHERE
    DATE(payment_date) BETWEEN '2005-05-24' AND '2005-05-25';
```

This query retrieves all records from the payments table where the payment_date is between May 24, 2005 and May 25,

2005.

As you can see, the `BETWEEN` operator is inclusive, meaning that the values specified in the `BETWEEN` operator are
included in the result set.

## Datetime

```sql
SELECT
  *
FROM
    payments
WHERE
    DATETIME(payment_date) = '2005-05-25 04:36:26';
```

This query retrieves all records from the payments table where the payment_date is equal to May 25, 2005 at 4:36:26 AM.
Note that this time. we use the `DATETIME()` function to extract the date and time part of the payment_date column as it
is a timestamp.

```sql
SELECT
  *
FROM
    payments
WHERE
    DATETIME(payment_date) BETWEEN '2005-05-25 16:00:00' AND '2005-05-25 17:00:00';
```

This query retrieves all records from the payments table where the payment_date is between May 25, 2005 at 4:00:00 PM
and May 25, 2005 at 5:00:00 PM.
