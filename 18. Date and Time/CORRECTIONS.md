# Date and Time

**Retrieve all rentals which were returned on the 29th of May 2005.**

Answer: 83

```sql
SELECT
    COUNT(*)
FROM
    rentals
WHERE
    DATE(return_date) = '2005-05-29';
```

**Retrieve all rentals which were done between 8:00 AM and 9:00 AM on the 28th of May 2005.**

Answer: 9

```sql
SELECT
  COUNT(*)
FROM
  rentals
WHERE
  DATETIME(rental_date) BETWEEN '2005-05-28 08:00:00' AND '2005-05-28 09:00:00';
```