To ensure a seamless and fruitful experience, we kindly ask you to complete a quick pre-requirements before the tutorial kicks off. You'll need to install a specific application that will be used to complete the hands-on activities and interactive sessions.


Application: 

**Beekeeper Studio Community Edition**

Description: 

Beekeeper Studio is a cross-platform SQL editor and database manager available for Linux, Mac, and Windows. Beekeeper Studio Community Edition is GPL licensed so it is free (libre) and free (gratis).

How to install Beekeeper Studio: 

Download the community edition from the releases page: https://github.com/beekeeper-studio/beekeeper-studio/releases/latest. Make sure to download the correct version according to your operating system. Once downloaded, simply double click on the binary and you should be able to install the application by following the instructions.


We understand that technology can be a bit tricky at times. If you encounter any installation challenges, don't worry - we're here to help. Simply come a bit before the beginning of the tutorial (~30 min before) and we will provide support. 

